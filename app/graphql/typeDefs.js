
import { gql } from "apollo-server-express";


export const typeDefs = gql`

# schema{
#  query: Query
#  mutatiob
# }
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  # This "Book" type defines the queryable fields for every book in our data source.
  type Book {
    title: String
    author: String
  }

  type Post{
    id:ID!
    body:String!
    createdAt:String!
    username: String!
    comments:[Commnet]!
    likes:[LikePost]!
    likeCount: Int
    commentCount: Int
  }

  type Commnet{
    id: ID!
    body:String!
    username:String!
    createdAt: String!

  }

  type User{
    id: ID!
    username:String!
    email:String
    createdAt: String
    token: String!
  }

  type LikePost{
    id:ID!
    username:String!
    createdAt:String!
  }

  input RegisterInput {
      username: String!
      password: String!
      confirmPassword: String!
      email: String!
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  # case, the "books" query returns an array of zero or more Books (defined above).
  type Query {
    books: [Book]
    getPosts:[Post]
    getPost(postId:ID!):Post
  }

  type Mutation {
    register(registerInput: RegisterInput): User!
    login(username: String!, password: String!): User!
    createPost(body:String!):Post!
    deletePost(postId:ID):String!
    createComment(postId:ID!, body:String!):Post!
    deleteComment(postId:ID!,commentId:String!):Post!
    likePost(postId:ID!):Post!
  }

  type Subscription {
    newPost:Post!
  }
`;
