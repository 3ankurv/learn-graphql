import {postResolver} from "./posts";
import { userResolver } from "./user";

 const resolvers = {
  Post:{
    likeCount:(parent)=>parent.likes.length,
    commentCount:(parent)=>parent.comments.length
  },
    Query: {
        ...postResolver.Query,
      },
      Mutation:{
        ...userResolver.Mutation,
        ...postResolver.Mutation
      },
      Subscription:{
        ...postResolver.Subscription
      }
}

 export default resolvers;