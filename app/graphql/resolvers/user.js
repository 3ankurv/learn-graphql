import { userModel as User } from "../../models/User";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { UserInputError } from "apollo-server-express";
import { validateRegisterInput, validateLoginInput } from "../../util/validator";

const generateToken= (user)=>{
   return  jwt.sign({
        id:user.id,
        username:user.username,
        email:user.email
    },'SECRET_KEY',{expiresIn:'1h'})
}

export const userResolver = {
    Mutation: {

        async register(_, { registerInput: { username, email, password, confirmPassword } }, context, info) {


            const { errors, valid} =validateRegisterInput(username, email, password, confirmPassword );

            if(!valid){
                throw new UserInputError('errors',{errors});
            }

            const user = await User.findOne({username});
            if(user){
                throw new UserInputError("This username is alrady taken",{
                    error:{
                        username:"its alrady exists."
                    }
                })
            }

                const newPassword = await bcrypt.hash(password,12);
                const newUser = new User({
                    username,
                    email,
                    password: newPassword,
                    createdAt: new Date().toISOString()
                });

                const res = await newUser.save();

                const token = generateToken(res)

                return {
                    ...res._doc,
                    id:res._id,
                    token
                }
        },

        async login(parent,{username,password},context,info){

            const {errors,valid} = validateLoginInput(username,password);
            if(!valid){
                 throw new UserInputError('errors',{
                     errors
                 })
            }
            const user = await User.findOne({username});
            if(!user){
                throw new UserInputError('User not found',{
                    error:{
                        msg:"User not found"
                    }
                })
            }

            const isValid = await bcrypt.compare(password,user.password);
            if(!isValid){
                throw new UserInputError('Invalid Credential',{
                    error:{
                        msg:"Invalid Credential"
                    }
                })
            }


            const token = generateToken(user)

                return {
                    ...user._doc,
                    id:user._id,
                    token
                }



        }

    }
}