import { ApolloServer, gql, PubSub } from "apollo-server-express";
const http = require("http");
import express from "express";
import mongoose from "mongoose";
import { postModel } from "./models/Post";
import { typeDefs } from "./graphql/typeDefs";

import resolvers from "./graphql/resolvers/index";

//  const gql = require('graphql-tag');
const port = process.env.PORT || 5000;
const MONGO_URL =
  "mongodb+srv://test-user:hello_world@cluster0.cguwb.mongodb.net/gqldata?retryWrites=true&w=majority";

const app = express();
const pubsub = new PubSub();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
  context: ({ req }) => ({ req, pubsub }),
});

mongoose.connect(MONGO_URL, { useNewUrlParser: true }).then(() => {
  console.log("MongoDB Connected");
});

//

server.applyMiddleware({ app });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

// The `listen` method launches a web server.
// app.listen({port},(url)=>{
//  console.log(`🚀  Server ready at ${port}`);
// })

httpServer.listen(port, () => {
  console.log(
    `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
  );
  console.log(
    `🚀 Subscriptions ready at ws://localhost:${port}${server.subscriptionsPath}`
  );
});
