
import  jwt from  "jsonwebtoken";
import { AuthenticationError } from "apollo-server-express";

export const  validateRegisterInput = (username, email, password, confirmPassword) =>{

    const errors={};
    if(username.trim()===''){
        errors.username = "username is required."
    }

    if(email.trim()===''){
        errors.email = "email is required."
    }else{
        const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
        if(!email.match(regEx)){
            errors.email = "Enter the valid email." 
        }
    }

    if(password.trim()===''){
        errors.password = "password is required";
    }

    else if( password !== confirmPassword){
        errors.confirmPassword = 'password & confirm password is not matched'
    }

   return {
       errors,
       valid: Object.keys(errors).length < 1
   }
}

export const validateLoginInput = (username,password) =>{
 
    const errors={};
    if(username.trim()===''){
        errors.username = "username is required."
    }
    if(password.trim()===''){
        errors.password = "password is required";
    }

    return {
        errors,
        valid: Object.keys(errors).length < 1
    }
}

export const validateUser = (context)=>{

const authHeader = context.req.headers.authorization;
if(authHeader){
    const token = authHeader.split("Bearer ")[1];
    if(token){
        try{
            const user = jwt.verify(token, 'SECRET_KEY');
            return user;
        }catch(err){
            throw new AuthenticationError('Invalid/Expired token')
        }  
    }
    throw new Error('Authentication token must be provided \' Bearer [toekn]')
}
throw new Error('Authentication header must be provided');
}