
import {postModel} from "../../models/Post";
import { validateUser } from "../../util/validator";
import { UserInputError } from "apollo-server-express";

const books = [
    {
      title: 'Harry Potter and the Chamber of Secrets',
      author: 'J.K. Rowling',
    },
    {
      title: 'Jurassic Park',
      author: 'Michael Crichton',
    },
   ];

export const postResolver = {
    Query: {
     books: () => books,
     async getPosts(){
       try{
         const posts =  await postModel.find();
          return posts;
       }catch(e){
         console.log(e)
         throw new Error(e);
       }
     },
     async getPost(_,{postId}){
       try{
        const post = await postModel.findById(postId);
        if(!post){
           throw new Error("Invalid post id")
        }
        return post;
       }catch(err){
          throw new Error(err)
       }
     }
   },
   Mutation:{

    async createPost(_,{body},context,info){

        try{
          const user = validateUser(context);
          console.log(user)

		if(body.trim()===''){
	throw new Error("Post body must not be empty.")
}

          const newPost =  new postModel({
            body,
            user:user.id,
            createdAt: new Date().toISOString(),
            username:user.username
          });
          const post = await newPost.save();
          context.pubsub.publish('NEW_POST',{
            newPost: post
          });
          return post;
        }catch(err){
          throw new Error(err)
        }
     
    },
    async deletePost(_,{postId},context,info){
      try{
        console.log(postId)
       const user = validateUser(context);
       console.log(user)
       const post = await postModel.findById(postId);
      
       console.log(post)
       if(!post){
         throw new Error("Invalid post Id")
       }
 
       if(user.username === post.username){
         await post.delete();
         return "Post deleted successfully."
       }
       post
        throw new Error("Action now allowed")
 
      }catch(err){
       throw new Error(err)
      }
    },

    async createComment(_,{postId, body},context,info){


      if(body.trim()===""){
        throw new UserInputError('Commnet can not be empty',{
          error:{
            msg:"Comment can not be blank."
          }
        })
      }

      const {username} = validateUser(context);
      const post = await postModel.findById(postId);
      if(post){
        
        post.comments.unshift({
          body,
          username,
          createdAt: new Date().toISOString()
        });

         await post.save();
         return post;
      }
      throw new Error("Invalid post Id")     
    }
,
  async deleteComment(_,{postId, commentId}, context,info){
    const {username} = validateUser(context);
    const post = await postModel.findById(postId);
    console.log(post)
    if(post){
      const postIdx =  post.comments.findIndex(comment=> comment.id === commentId)

      if(username === post.comments[postIdx]["username"]){
        post.comments.splice(postIdx,1);
        await post.save();
        return post;
      }
      throw new Error("Action not allowed")   
    }
    throw new Error("Invalid post Id OR Comment ID")     
  },

  async likePost(_,{postId},context,info){

    try{

      const {username} = validateUser(context);
    const post = await postModel.findById(postId);

    if(post){
      
      const likeData =  post.likes.filter(like=> like.username === username);
      if(likeData.length){
        console.log(likeData[0]['id'])
        console.log(post.id)
       const likeIdx =  post.likes.findIndex((f)=>f.id===likeData[0]['id']);
       post.likes.splice(likeIdx,1);
       //unlike the post
       await post.save();
       return post;
      }

      await post.likes.unshift({
        username,
        createdAt: new Date().toISOString()
      });
      // like the post
      await post.save();
      return post;

    }

    throw new Error('Invalid post Id')

    }
    catch(e){
      throw new Error(e);
    }
    
  }

   },

   Subscription:{
     newPost:{
      subscribe:(_,args,{pubsub})=> pubsub.asyncIterator('NEW_POST')
     }
   }
   
   };

  // export default postResolver;
